﻿using Domain;
using LibraryTask.Models.SqlModels;
using LibraryTask.Repositories.SqlRepositories;
using LibraryTask.Repositories.SqlRepositories.Interfaces;
using LibraryTask.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTask.Services.SqlServices
{
    public class BookService : IService<DomainBook>
    {
        IBookRepository _iBookRepo;
        IAuthorRepository _iAuthorRepo;
        ApplicationContext _context;

        public BookService(ApplicationContext context, IBookRepository iBookRepo, IAuthorRepository iAuthorRepo)
        {
            _iBookRepo = iBookRepo;
            _iAuthorRepo = iAuthorRepo;
            _context = context;
        }

        public async Task<DomainBook> Create(DomainBook model, List<Guid> authors)
        {
            try
            {
                BookModel book = new BookModel() 
                { 
                    Title = model.Title,
                    Genre = model.Genre,
                    Publisher = model.Publisher,
                    Year = model.Year
                };

                book = await _iBookRepo.Create(book);

                Repository <BookAuthorModel> _booAuthorRepo = new Repository<BookAuthorModel>(_context);

                if (authors != null)
                {
                    foreach (Guid id in authors)
                    {
                        AuthorModel author = await _iAuthorRepo.Get(id);
                        if (author != null)
                        {
                            await _booAuthorRepo.Create(new BookAuthorModel()
                            {
                                Author = author,
                                Book = book
                            });
                        }
                    }
                }

                model.Id = book.Id;

                return model;
            }catch(Exception ex)
            { 
                throw ex; 
            }
        }

        public async Task<Guid> Delete(Guid id)
        {
            try
            {
                BookModel book = await _iBookRepo.Get(id);

                if (book != null)
                {
                    await _iBookRepo.Delete(book);
                    return book.Id;
                }
                else
                {
                    throw new Exception("No such book!");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DomainBook> Edit(DomainBook model, List<Guid> authors)
        {
            try
            {
                BookModel book = await _iBookRepo.GetById(model.Id);
                book.Title = model.Title;
                book.Genre = model.Genre;
                book.Year = model.Year;
                book.Publisher = model.Publisher;

                List<BookAuthorModel> list = book.BookAuthor.ToList();
                Repository<BookAuthorModel> _booAuthorRepo = new Repository<BookAuthorModel>(_context);

                foreach (var bookAuthor in list)
                {
                    await _booAuthorRepo.Delete(bookAuthor);
                }

                foreach (Guid id in authors)
                {
                    AuthorModel author = await _iAuthorRepo.Get(id);
                    if (author != null)
                    {
                        await _booAuthorRepo.Create(new BookAuthorModel()
                        {
                            Author = author,
                            Book = book
                        });
                    }
                }

                await _iBookRepo.Edit(book);

                return model;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<DomainBook>> GetList(int skip, int take)
        {
            List<BookModel> booksList = await _iBookRepo.GetList(skip, take);
            List<DomainBook> domainBooks = new List<DomainBook>();

            foreach(var book in booksList)
            {
                DomainBook domainAuthor = new DomainBook()
                {
                    Id = book.Id,
                    Title = book.Title,
                    Publisher = book.Publisher,
                    Genre = book.Genre,
                    Year = book.Year
                };

                foreach (var author in book.BookAuthor)
                {
                    domainAuthor.BookAuthor.Add(new DomainAuthor()
                    {
                        Id = author.Author.Id,
                        FirstName = author.Author.FirstName,
                        LastName = author.Author.LastName,
                        Country = author.Author.Country
                    });
                }

                domainBooks.Add(domainAuthor);
            }

            return domainBooks;
        }
        
        public async Task<long> GetCount()
        {
            long count = await _iBookRepo.GetCount();
            return count;
        }

        public async Task<DomainBook> Get(Guid id)
        {
            BookModel book = await _iBookRepo.GetById(id);

            DomainBook domainBook = new DomainBook()
            {
                Id = book.Id,
                Title = book.Title,
                Publisher = book.Publisher,
                Genre = book.Genre,
                Year = book.Year
            };

            foreach (var item in book.BookAuthor)
            {
                domainBook.BookAuthor.Add(new DomainAuthor()
                {
                    Id = item.Author.Id,
                    FirstName = item.Author.FirstName,
                    LastName = item.Author.LastName,
                    Country = item.Author.Country
                });
            }

            return domainBook;
        }
    }
}
