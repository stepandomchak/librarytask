﻿using Domain;
using LibraryTask.Models.SqlModels;
using LibraryTask.Repositories.SqlRepositories;
using LibraryTask.Repositories.SqlRepositories.Interfaces;
using LibraryTask.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTask.Services.SqlServices
{
    public class AuthorService : IService<DomainAuthor> 
    {
        IBookRepository _iBookRepo;
        IAuthorRepository _iAuthorRepo;
        ApplicationContext _context;

        public AuthorService(ApplicationContext context, IBookRepository iBookRepo, IAuthorRepository iAuthorRepo)
        {
            _context = context;
            _iBookRepo = iBookRepo;
            _iAuthorRepo = iAuthorRepo;
        }

        public async Task<DomainAuthor> Create(DomainAuthor model, List<Guid> books)
        {
            try
            {
                AuthorModel author = await _iAuthorRepo.GetAuthorByName(model.FirstName, model.LastName);
                Repository<BookAuthorModel> _booAuthorRepo = new Repository<BookAuthorModel>(_context);

                if (author == null)
                {
                    author = new AuthorModel()
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Country = model.Country
                    };
                    author = await _iAuthorRepo.Create(author);
                }
                else
                    throw new Exception("ExistenAuthor");

                if (books != null)
                {
                    foreach (Guid id in books)
                    {
                        BookModel book = await _iBookRepo.Get(id);
                        if (book != null)
                        {
                            await _booAuthorRepo.Create(new BookAuthorModel() { Author = author, Book = book });
                        }
                    }
                }

                model.Id = author.Id;
                return model;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Guid> Delete(Guid id)
        {
            try
            {
                AuthorModel author = await _iAuthorRepo.Get(id);
                if (author != null)
                {
                    await _iAuthorRepo.Delete(author);
                    return author.Id;
                }
                else
                    throw new Exception("No such author");
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DomainAuthor> Edit(DomainAuthor model, List<Guid> books)
        {
            try
            {
                AuthorModel author = await _iAuthorRepo.GetById(model.Id);
                author.FirstName = model.FirstName;
                author.LastName = model.LastName;
                author.Country = model.Country;

                List<BookAuthorModel> list = author.BookAuthor.ToList();
                Repository<BookAuthorModel> _booAuthorRepo = new Repository<BookAuthorModel>(_context);

                foreach (var bookAuthor in list)
                {
                    await _booAuthorRepo.Delete(bookAuthor);
                }

                foreach (Guid id in books)
                {
                    BookModel book = await _iBookRepo.Get(id);
                    if (book != null)
                    {
                        await _booAuthorRepo.Create(new BookAuthorModel()
                        {
                            Author = author,
                            Book = book
                        });
                    }
                }

                author = await _iAuthorRepo.Edit(author);
                model.Id = author.Id;
                
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<DomainAuthor>> GetList(int skip, int take)
        {
            List<AuthorModel> authorsList = await _iAuthorRepo.GetList(skip, take);
            List<DomainAuthor> domainAuthors= new List<DomainAuthor>();

            foreach(var author in authorsList)
            {
                DomainAuthor domainAuthor = new DomainAuthor()
                {
                    Id = author.Id,
                    FirstName = author.FirstName,
                    LastName = author.LastName,
                    Country = author.Country
                };
                
                foreach(var book in author.BookAuthor)
                {
                    domainAuthor.BookAuthor.Add(new DomainBook()
                    {
                        Id = book.Book.Id,
                        Title = book.Book.Title,
                        Genre = book.Book.Genre,
                        Publisher = book.Book.Publisher,
                        Year = book.Book.Year
                    });
                }

                domainAuthors.Add(domainAuthor);
            }

            return domainAuthors;
        }

        public async Task<long> GetCount()
        {
            long count = await _iAuthorRepo.GetCount();
            return count;
        }

        public async Task<DomainAuthor> Get(Guid id)
        {
            AuthorModel author = await _iAuthorRepo.GetById(id);
            DomainAuthor domainAuthor = new DomainAuthor()
            {
                Id = author.Id,
                FirstName = author.FirstName,
                LastName = author.LastName,
                Country = author.Country
            };

            foreach (var item in author.BookAuthor)
            {
                domainAuthor.BookAuthor.Add(new DomainBook()
                {
                    Id = item.Book.Id,
                    Title = item.Book.Title,
                    Genre = item.Book.Genre,
                    Publisher = item.Book.Publisher,
                    Year = item.Book.Year
                });
            }

            return domainAuthor;
        }

    }
}
