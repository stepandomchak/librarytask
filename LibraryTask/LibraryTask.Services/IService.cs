﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryTask.Services
{
    public interface IService<TEntity>
    {
        Task<TEntity> Get(Guid id);

        Task<TEntity> Create(TEntity model, List<Guid> ids);

        Task<TEntity> Edit(TEntity model, List<Guid> ids);

        Task<Guid> Delete(Guid id);

        Task<List<TEntity>> GetList(int skip, int take);

        Task<long> GetCount();

    }
}
