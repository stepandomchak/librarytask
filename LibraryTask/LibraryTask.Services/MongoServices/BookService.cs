﻿using Domain;
using LibraryTask.Models.MongoModels;
using LibraryTask.Repositories.MongoRepositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryTask.Services.MongoServices
{
    public class BookService : IService<DomainBook>
    {
        IBookRepository _iBookRepo;
        IAuthorRepository _iAuthorRepo;

        public BookService(IBookRepository iBookRepo, IAuthorRepository iAuthorRepo)
        {
            _iBookRepo = iBookRepo;
            _iAuthorRepo = iAuthorRepo;
        }

        public async Task<DomainBook> Create(DomainBook item, List<Guid> ids)
        {
            BookModel book = new BookModel()
            {
                Title = item.Title,
                Publisher = item.Publisher,
                Genre = item.Genre,
                Year = item.Year
            };

            book.Authors = ids;
            book = await _iBookRepo.Create(book);

            if (ids != null)
            {
                foreach (Guid authorId in ids)
                {
                    AuthorModel author = await _iAuthorRepo.Get(authorId);
                    author.Books.Add(book.Id);
                    await _iAuthorRepo.Edit(author);
                }
            }

            item.Id = book.Id;

            return item;
        }

        public async Task<Guid> Delete(Guid id)
        {
            var book = await _iBookRepo.Get(id);
            var authors = await _iAuthorRepo.GetBookAuthors(id);

            foreach(var author in authors)
            {
                author.Books.Remove(book.Id);
                await _iAuthorRepo.Edit(author);
            }

            return await _iBookRepo.Delete(book);
        }

        public async Task<DomainBook> Edit(DomainBook item, List<Guid> ids)
        {
            var book = await _iBookRepo.Get(item.Id);

            book.Title = item.Title;
            book.Genre = item.Genre;
            book.Publisher = item.Publisher;
            book.Year = item.Year;

            foreach (var id in book.Authors)
            {
                if (!ids.Contains(id))
                {
                    var author = await _iAuthorRepo.Get(id);
                    author.Books.Remove(book.Id);
                    await _iAuthorRepo.Edit(author);
                }
            }

            foreach (Guid authorId in ids)
            {
                if (!book.Authors.Contains(authorId))
                {
                    AuthorModel author = await _iAuthorRepo.Get(authorId);
                    author.Books.Add(book.Id);
                    await _iAuthorRepo.Edit(author);
                }
            }
            book.Authors = ids;
            await _iBookRepo.Edit(book);
            return item;
        }

        public async Task<DomainBook> Get(Guid id)
        {
            var book = await _iBookRepo.Get(id);
            book.BookAuthor = await _iAuthorRepo.GetBookAuthors(id);

            DomainBook domainBook = new DomainBook()
            {
                Id = book.Id,
                Title = book.Title,
                Publisher = book.Publisher,
                Genre = book.Genre,
                Year = book.Year
            };

            foreach(var item in book.BookAuthor)
            {
                domainBook.BookAuthor.Add(new DomainAuthor()
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Country = item.Country
                });
            }

            return domainBook;
        }

        public async Task<long> GetCount()
        {
            return await _iBookRepo.GetCount();
        }

        public async Task<List<DomainBook>> GetList(int skip, int take)
        {
            List<BookModel> booksList = await _iBookRepo.GetList(skip, take);
            List<DomainBook> domainBooks = new List<DomainBook>();

            foreach (var book in booksList)
            {
                DomainBook domainAuthor = new DomainBook()
                {
                    Id = book.Id,
                    Title = book.Title,
                    Publisher = book.Publisher,
                    Genre = book.Genre,
                    Year = book.Year
                };

                foreach (var author in book.BookAuthor)
                {
                    domainAuthor.BookAuthor.Add(new DomainAuthor()
                    {
                        Id = author.Id,
                        FirstName = author.FirstName,
                        LastName = author.LastName,
                        Country = author.Country
                    });
                }

                domainBooks.Add(domainAuthor);
            }

            return domainBooks;
        }

    }
}
