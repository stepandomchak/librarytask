﻿using Domain;
using LibraryTask.Models.MongoModels;
using LibraryTask.Repositories.MongoRepositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryTask.Services.MongoServices
{
    public class AuthorService : IService<DomainAuthor>
    {
        IBookRepository _iBookRepo;
        IAuthorRepository _iAuthorRepo;

        public AuthorService(IBookRepository iBookRepo, IAuthorRepository iAuthorRepo)
        {
            _iBookRepo = iBookRepo;
            _iAuthorRepo = iAuthorRepo;
        }

        public async Task<DomainAuthor> Create(DomainAuthor item, List<Guid> ids = null)
        {
            AuthorModel author = new AuthorModel()
            {
                FirstName = item.FirstName,
                LastName = item.LastName,
                Country = item.Country
            };

            author.Books = ids;
            await _iAuthorRepo.Create(author);

            if (ids != null)
            {
                foreach (Guid bookId in ids)
                {
                    BookModel book = await _iBookRepo.Get(bookId);
                    book.Authors.Add(author.Id);
                    await _iBookRepo.Edit(book);
                }
            }

            item.Id = author.Id;

            return item;
        }

        public async Task<Guid> Delete(Guid id)
        {
            var author = await _iAuthorRepo.Get(id);
            var books = await _iBookRepo.GetAuthorBooks(id);

            foreach (var book in books)
            {
                book.Authors.Remove(author.Id);
                await _iBookRepo.Edit(book);
            }

            return await _iAuthorRepo.Delete(author);
        }

        public async Task<DomainAuthor> Edit(DomainAuthor item, List<Guid> ids)
        {
            var author = await _iAuthorRepo.Get(item.Id);

            author.FirstName = item.FirstName;
            author.LastName = item.LastName;
            author.Country = item.Country;

            foreach (var id in author.Books)
            {
                if (!ids.Contains(id))
                {
                    var book = await _iBookRepo.Get(id);
                    book.Authors.Remove(author.Id);
                    await _iBookRepo.Edit(book);
                }
            }

            foreach (Guid bookId in ids)
            {
                if (!author.Books.Contains(bookId))
                {
                    BookModel book = await _iBookRepo.Get(bookId);
                    book.Authors.Add(author.Id);
                    await _iBookRepo.Edit(book);
                }
            }
            author.Books = ids;
            await _iAuthorRepo.Edit(author);
            return item;
        }

        public async Task<DomainAuthor> Get(Guid id)
        {
            var author = await _iAuthorRepo.Get(id);
            author.BooksList = await _iBookRepo.GetAuthorBooks(id);

            DomainAuthor domainAuthor = new DomainAuthor()
            {
                Id = author.Id,
                FirstName = author.FirstName,
                LastName = author.LastName,
                Country = author.Country
            };
            foreach (var book in author.BooksList)
            {
                domainAuthor.BookAuthor.Add(new DomainBook()
                {
                    Id = book.Id,
                    Title = book.Title,
                    Genre = book.Genre,
                    Publisher = book.Publisher,
                    Year = book.Year
                });
            }

            return domainAuthor;
        }

        public async Task<long> GetCount()
        {
            return await _iAuthorRepo.GetCount();
        }

        public async Task<List<DomainAuthor>> GetList(int skip, int take)
        {
            List<AuthorModel> authorsList = await _iAuthorRepo.GetList(skip, take);
            List<DomainAuthor> domainAuthors = new List<DomainAuthor>();

            foreach(var author in authorsList)
            {
                DomainAuthor domainAuthor = new DomainAuthor()
                {
                    Id = author.Id,
                    FirstName = author.FirstName,
                    LastName = author.LastName,
                    Country = author.Country
                };
                domainAuthors.Add(domainAuthor);
            }

            return domainAuthors;
        }

    }
}
