﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class DomainAuthor
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public List<DomainBook> BookAuthor { get; set; }

        public DomainAuthor()
        {
            this.BookAuthor = new List<DomainBook>();
        }
    }
}
