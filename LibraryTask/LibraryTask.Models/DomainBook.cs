﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class DomainBook
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public DateTime Year { get; set; }

        public string Publisher { get; set; }

        public List<DomainAuthor> BookAuthor { get; set; }

        public DomainBook()
        {
            BookAuthor = new List<DomainAuthor>();
        }
    }
}
