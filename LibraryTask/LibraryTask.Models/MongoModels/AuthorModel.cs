﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryTask.Models.MongoModels
{
    public class AuthorModel : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public ICollection<Guid> Books { get; set; }
        
        [BsonIgnore]
        public ICollection<BookModel> BooksList { get; set; }

        public AuthorModel()
        {
            Books = new List<Guid>();
            BooksList = new List<BookModel>();
        }

    }
}
