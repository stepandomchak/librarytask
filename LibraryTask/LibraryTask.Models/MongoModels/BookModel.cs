﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryTask.Models.MongoModels
{
    public class BookModel : BaseEntity
    {        
        public string Title { get; set; }
                
        public string Genre { get; set; }

        public DateTime Year { get; set; }

        public string Publisher { get; set; }

        public ICollection<Guid> Authors { get; set; }

        [BsonIgnore]
        public ICollection<AuthorModel> BookAuthor { get; set; }

        public BookModel()
        {
            Authors = new List<Guid>();
            BookAuthor = new List<AuthorModel>();
        }

    }
}
