﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryTask.Models.MongoModels
{
    public class LibraryDbSettings : ILibraryDatabaseSettings
    {
        public string BooksCollectionName { get; set; }

        public string AuthorsCollectionName { get; set; }

        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }
    }

    public interface ILibraryDatabaseSettings
    {
        string BooksCollectionName { get; set; }

        string AuthorsCollectionName { get; set; }

        string ConnectionString { get; set; }

        string DatabaseName { get; set; }
    }

}
