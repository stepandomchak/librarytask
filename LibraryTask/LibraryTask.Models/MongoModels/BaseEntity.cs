﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryTask.Models.MongoModels
{
    public class BaseEntity
    {
        [BsonId]
        [BsonDefaultValue(GuidRepresentation.Standard)]
        public Guid Id { get; set; }
    }
   
}
