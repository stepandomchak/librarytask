﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibraryTask.Models.SqlModels
{
    public class BookModel : BaseEntity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Genre { get; set; }

        public DateTime Year { get; set; }

        public string Publisher { get; set; }

        public virtual ICollection<BookAuthorModel> BookAuthor { get; set; }

        public BookModel()
        {
            BookAuthor = new List<BookAuthorModel>();
        }

    }
}
