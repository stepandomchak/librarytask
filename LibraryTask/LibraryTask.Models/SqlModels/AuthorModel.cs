﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibraryTask.Models.SqlModels
{
    public class AuthorModel : BaseEntity
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Country { get; set; }

        public virtual ICollection<BookAuthorModel> BookAuthor { get; set; }

        public AuthorModel()
        {
            BookAuthor = new List<BookAuthorModel>();
        }

    }
}
