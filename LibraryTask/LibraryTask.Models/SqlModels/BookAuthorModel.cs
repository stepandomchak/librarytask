﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryTask.Models.SqlModels
{
    public class BookAuthorModel : BaseEntity
    {
        public Guid BookId { get; set; }

        public virtual BookModel Book { get; set; }

        public Guid AuthorId { get; set; }

        public virtual AuthorModel Author { get; set; }

    }
}
