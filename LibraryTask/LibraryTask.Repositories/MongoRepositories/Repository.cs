﻿using LibraryTask.Models.MongoModels;
using LibraryTask.Storage;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.MongoRepositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly IMongoContext _mongoContext;
        protected IMongoCollection<TEntity> _dbCollection;

        protected Repository(IMongoContext context)
        {
            _mongoContext = context;
            _dbCollection = _mongoContext.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public async Task<TEntity> Create(TEntity item)
        {
            if (item == null)
            {
                return null;
            }
            _dbCollection = _mongoContext.GetCollection<TEntity>(typeof(TEntity).Name);
            await _dbCollection.InsertOneAsync(item);
            return item;
        }

        public async Task<Guid> Delete(TEntity item)
        {
            await _dbCollection.DeleteOneAsync(Builders<TEntity>.Filter.Eq("_id", item.Id));
            return item.Id;
        }

        public async Task<TEntity> Edit(TEntity item)
        {
            var filter = Builders<TEntity>.Filter.Eq(c => c.Id, item.Id);
            await _dbCollection.ReplaceOneAsync(filter, item);      

            return item;
        }

        public async Task<TEntity> Get(Guid id)
        {
            FilterDefinition<TEntity> filter = Builders<TEntity>.Filter.Eq("_id", id);
            _dbCollection = _mongoContext.GetCollection<TEntity>(typeof(TEntity).Name);

            return await _dbCollection.FindAsync(filter).Result.FirstOrDefaultAsync();
        }

        public async Task<long> GetCount()
        {
            return await _dbCollection.Find(Builders<TEntity>.Filter.Empty).CountDocumentsAsync();            
        }

        public async Task<List<TEntity>> GetList(int skip, int take)
        {
            var collection = _mongoContext.GetCollection<TEntity>(typeof(TEntity).Name);
            var all = collection.Find(Builders<TEntity>.Filter.Empty).Skip(skip).Limit(take);
            return await all.ToListAsync();
        }
    }
}
