﻿using LibraryTask.Models.MongoModels;
using LibraryTask.Repositories.MongoRepositories.Interfaces;
using LibraryTask.Storage;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.MongoRepositories
{
    public class BookRepository : Repository<BookModel>, IBookRepository
    {
        public BookRepository(IMongoContext context) : base(context)
        { }

        public async Task<List<BookModel>> GetAuthorBooks(Guid authorId)
        {
            return await Task.Run(() =>
            {
                var result = _dbCollection.Find(s => s.Authors.Contains(authorId)).ToListAsync();
                return result;
            }).ConfigureAwait(false);
        }

    }
}
