﻿using LibraryTask.Models.MongoModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.MongoRepositories.Interfaces
{
    public interface IBookRepository : IRepository<BookModel>
    {
        Task<List<BookModel>> GetAuthorBooks(Guid authorId);
    }
}
