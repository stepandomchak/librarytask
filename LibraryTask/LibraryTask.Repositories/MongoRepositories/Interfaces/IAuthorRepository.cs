﻿using LibraryTask.Models.MongoModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.MongoRepositories.Interfaces
{
    public interface IAuthorRepository : IRepository<AuthorModel>
    {
        Task<List<AuthorModel>> GetBookAuthors(Guid bookId);
    }
}
