﻿using LibraryTask.Models.MongoModels;
using LibraryTask.Repositories.MongoRepositories.Interfaces;
using LibraryTask.Storage;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.MongoRepositories
{
    public class AuthorRepository : Repository<AuthorModel>, IAuthorRepository
    {
        public AuthorRepository(IMongoContext context) : base(context)
        {
        }

        public async Task<List<AuthorModel>> GetBookAuthors(Guid bookId)
        {
            return await Task.Run(() =>
            {
                var result = _dbCollection.Find(s => s.Books.Contains(bookId)).ToListAsync();
                return result;
            }).ConfigureAwait(false);
        }

    }
}
