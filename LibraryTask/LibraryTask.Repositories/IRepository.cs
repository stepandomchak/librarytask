﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryTask.Repositories
{
    public interface IRepository<TEntity>
    {
        Task<TEntity> Get(Guid id);

        Task<List<TEntity>> GetList(int skip, int take);

        Task<long> GetCount();

        Task<TEntity> Create(TEntity item);

        Task<TEntity> Edit(TEntity item);

        Task<Guid> Delete(TEntity item);
    }
}
