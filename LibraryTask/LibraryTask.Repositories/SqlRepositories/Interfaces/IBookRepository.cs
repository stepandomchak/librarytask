﻿using LibraryTask.Models.SqlModels;
using System;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.SqlRepositories.Interfaces
{
    public interface IBookRepository : IRepository<BookModel>
    {
        Task<BookModel> GetById(Guid id);
    }
}
