﻿using LibraryTask.Models.SqlModels;
using System;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.SqlRepositories.Interfaces
{
    public interface IAuthorRepository : IRepository<AuthorModel>
    {
        Task<AuthorModel> GetAuthorByName(string firstName, string lastName);

        Task<AuthorModel> GetById(Guid id);
    }
}
