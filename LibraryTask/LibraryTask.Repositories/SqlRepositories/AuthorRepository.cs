﻿using LibraryTask.Models.SqlModels;
using LibraryTask.Repositories.SqlRepositories.Interfaces;
using LibraryTask.Storage;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.SqlRepositories
{
    public class AuthorRepository : Repository<AuthorModel>, IAuthorRepository
    {
        public AuthorRepository(ApplicationContext context) : base(context)
        { }

        public async Task<AuthorModel> GetAuthorByName(string firstName, string lastName)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.FirstName == firstName && m.LastName == lastName).FirstOrDefault();
            }).ConfigureAwait(false);
        }

        public async Task<AuthorModel> GetById(Guid id)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.Id == id).Include(s => s.BookAuthor).ThenInclude(b => b.Book).FirstOrDefault();
            }).ConfigureAwait(false);
        }

    }
}
