﻿using LibraryTask.Models.SqlModels;
using LibraryTask.Storage;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.SqlRepositories
{
    public class Repository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly ApplicationContext _context;
        private DbSet<TEntity> dbSet;
        private bool disposed = false;


        public Repository(ApplicationContext context)
        {
            _context = context;
        }

        protected DbSet<TEntity> Entities
        {
            get
            {
                if (dbSet == null)
                    dbSet = _context.Set<TEntity>();
                return dbSet;
            }
        }

        public async Task<TEntity> Create(TEntity item)
        {
            this.Entities.Add(item);
            await _context.SaveChangesAsync().ConfigureAwait(false);
            return item;
        }

        public async Task<Guid> Delete(TEntity item)
        {
            _context.Entry(item).State = EntityState.Deleted;
            await _context.SaveChangesAsync().ConfigureAwait(false);
            return item.Id;
        }

        public async Task<TEntity> Edit(TEntity item)
        {
            if (!this.Entities.Local.Any(e => e == item))
                this.Entities.Attach(item);

            await this._context.SaveChangesAsync().ConfigureAwait(false);

            return item;
        }

        public async Task<TEntity> Get(Guid id)
        {
            return await Task.Run(() =>
            {
                return this.Entities.Where(s => s.Id == id).FirstOrDefault();
            }).ConfigureAwait(false);
        }
        
        public async Task<List<TEntity>> GetList(int skip, int take)
        {
            return await Task.Run(() =>
            {
                return Entities.Skip(skip).Take(take).ToListAsync();
            }).ConfigureAwait(false);
        }

        public async Task<long> GetCount()
        {
            return await Task.Run(() =>
            {
                return Convert.ToInt64(Entities.Count());
            }).ConfigureAwait(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }

}