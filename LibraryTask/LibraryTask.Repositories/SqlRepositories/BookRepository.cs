﻿using LibraryTask.Models.SqlModels;
using LibraryTask.Repositories.SqlRepositories.Interfaces;
using LibraryTask.Storage;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTask.Repositories.SqlRepositories
{
    public class BookRepository : Repository<BookModel>, IBookRepository
    {
        public BookRepository(ApplicationContext context) : base(context)
        { }

        public async Task<BookModel> GetById(Guid id)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.Id == id).Include(s => s.BookAuthor).ThenInclude(b => b.Author).FirstOrDefault();
            }).ConfigureAwait(false);
        }

    }
}
