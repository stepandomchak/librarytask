using LibraryTask.Models.SqlModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace LibraryTask.Tests
{
    [TestFixture]
    public class SqlTests
    {
        private readonly string url = "https://localhost:44308/api/SqlLibrary/";
        private Guid _bookId;
        private Guid _authorId;

        [SetUp]
        public void Setup()
        {

        }

        [Test, Order(1)]
        public async Task CreateBookTest()
        {
            using (var client = new HttpClient())
            {
                BookModel book = new BookModel()
                {
                    Title = "SqlTestBook",
                    Genre = "SqlTestGenre",
                    Year = DateTime.Now,
                    Publisher = "SqlTestPublisher"
                };

                var content = Newtonsoft.Json.JsonConvert.SerializeObject(book);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json");

                var response = await client.PostAsync(url + "createBook/", byteContent);
                var responseString = await response.Content.ReadAsStringAsync();

                book = Newtonsoft.Json.JsonConvert.DeserializeObject<BookModel>(responseString);
                _bookId = book.Id;

                Assert.IsTrue(book != null);
                Assert.Pass();
            }
        }

        [Test, Order(2)]
        public async Task GetBooksTest()
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url + "getBooksList?skip=0&take=5");
                var responseString = await response.Content.ReadAsStringAsync();

                List<BookModel> books = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BookModel>>(responseString);

                Assert.IsTrue(books.Count != 0);
                Assert.Pass();
            }
        }

        [Test, Order(3)]
        public async Task UpdateBookTest()
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url + "GetBook/" + _bookId);
                var responseString = await response.Content.ReadAsStringAsync();

                BookModel book = Newtonsoft.Json.JsonConvert.DeserializeObject<BookModel>(responseString);

                book.Title = "Sql Updated Title";

                var content = Newtonsoft.Json.JsonConvert.SerializeObject(book);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json");

                response = await client.PutAsync(url + "editBook/", byteContent);
                responseString = await response.Content.ReadAsStringAsync();

                book = Newtonsoft.Json.JsonConvert.DeserializeObject<BookModel>(responseString);
                
                Assert.IsTrue(book != null);
                Assert.Pass();
            }
        }

        [Test, Order(4)]
        public async Task DeleteBookTest()
        {
            using (var client = new HttpClient())
            {
                var response = await client.DeleteAsync(url + "deleteBook/" + _bookId);
                var responseString = await response.Content.ReadAsStringAsync();

                int bookId = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseString);

                Assert.IsTrue(bookId != 0);
                Assert.Pass();
            }
        }


        [Test, Order(5)]
        public async Task CreateAuthorTest()
        {
            using (var client = new HttpClient())
            {
                AuthorModel author = new AuthorModel()
                {
                    FirstName = "TestAuthorFirstName",
                    LastName= "TestAuthorLastName",
                    Country = "TestAuthorCountry"
                };

                var content = Newtonsoft.Json.JsonConvert.SerializeObject(author);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json");

                var response = await client.PostAsync(url + "createAuthor/", byteContent);
                var responseString = await response.Content.ReadAsStringAsync();

                author = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthorModel>(responseString);
                _authorId = author.Id;

                Assert.IsTrue(author != null);
                Assert.Pass();
            }
        }

        [Test, Order(6)]
        public async Task GetAuthorTest()
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url + "getAuthorsList?skip=0&take=5");
                var responseString = await response.Content.ReadAsStringAsync();

                List<AuthorModel> authors = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AuthorModel>>(responseString);

                Assert.IsTrue(authors.Count != 0);
                Assert.Pass();
            }
        }

        [Test, Order(7)]
        public async Task UpdateAuthorTest()
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url + "GetAuthor/" + _authorId);
                var responseString = await response.Content.ReadAsStringAsync();

                AuthorModel author = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthorModel>(responseString);

                author.FirstName = "Updated FirstName";

                var content = Newtonsoft.Json.JsonConvert.SerializeObject(author);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json");

                response = await client.PutAsync(url + "editAuthor/", byteContent);
                responseString = await response.Content.ReadAsStringAsync();

                author = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthorModel>(responseString);

                Assert.IsTrue(author != null);
                Assert.Pass();
            }
        }

        [Test, Order(8)]
        public async Task DeleteAuthorTest()
        {
            using (var client = new HttpClient())
            {
                var response = await client.DeleteAsync(url + "deleteAuthor/" + _authorId);
                var responseString = await response.Content.ReadAsStringAsync();

                int authorId = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(responseString);

                Assert.IsTrue(authorId != 0);
                Assert.Pass();
            }
        }

    }
}