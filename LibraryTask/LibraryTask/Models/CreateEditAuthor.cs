﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTask.Models
{
    public class CreateEditAuthor
    {
        public Guid id { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string country { get; set; }

        public List<Guid> books { get; set; }

        public CreateEditAuthor()
        {
            this.books = new List<Guid>(); 
        }

    }
}
