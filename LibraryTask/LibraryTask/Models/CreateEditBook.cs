﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTask.Models
{
    public class CreateEditBook
    {
        public Guid id { get; set; }

        public string title { get; set; }

        public string genre { get; set; }

        public DateTime year { get; set; }

        public string publisher { get; set; }

        public List<Guid> authors { get; set; }

        public CreateEditBook()
        {
            authors = new List<Guid>();
        }
    }
}
