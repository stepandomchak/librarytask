using LibraryTask.Models.MongoModels;
using LibraryTask.Storage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace LibraryTask
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews()
                    .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                );
         
            services.AddHttpContextAccessor();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            services.AddTransient<Services.SqlServices.AuthorService>();
            services.AddTransient<Services.SqlServices.BookService>();
            services.AddTransient<Services.MongoServices.AuthorService>();
            services.AddTransient<Services.MongoServices.BookService>();

            services.AddTransient<Func<Services.IService<Domain.DomainAuthor>>>(s => () =>
            {
                var dbType = s.GetService<IHttpContextAccessor>().HttpContext.Request.Headers["DbType"];

                if (dbType.Equals("SqlDb"))
                    return s.GetService<Services.SqlServices.AuthorService>();
                if(dbType.Equals("MongoDb"))
                    return s.GetService<Services.MongoServices.AuthorService>();

                return null;
            });

            services.AddTransient<Func<Services.IService<Domain.DomainBook>>>(s => () =>
            {
                var dbType = s.GetService<IHttpContextAccessor>().HttpContext.Request.Headers["DbType"];

                if (dbType.Equals("SqlDb"))
                    return s.GetService<Services.SqlServices.BookService>();
                if (dbType.Equals("MongoDb"))
                    return s.GetService<Services.MongoServices.BookService>();
                
                return null;
            });

            services.AddTransient<Repositories.SqlRepositories.Interfaces.IBookRepository,
                Repositories.SqlRepositories.BookRepository>();
            services.AddTransient<Repositories.SqlRepositories.Interfaces.IAuthorRepository,
                Repositories.SqlRepositories.AuthorRepository>();


            services.AddTransient<IMongoContext, MongoContext>();
            services.AddTransient<Repositories.MongoRepositories.Interfaces.IBookRepository,
                Repositories.MongoRepositories.BookRepository>();
            services.AddTransient<Repositories.MongoRepositories.Interfaces.IAuthorRepository,
                Repositories.MongoRepositories.AuthorRepository>();


            services.AddDbContext<ApplicationContext>(s => {
                s.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                });
            
            services.Configure<LibraryDbSettings>(options =>
            {
                options.ConnectionString
                    = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                options.DatabaseName
                    = Configuration.GetSection("MongoConnection:DatabaseName").Value;
            });

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
