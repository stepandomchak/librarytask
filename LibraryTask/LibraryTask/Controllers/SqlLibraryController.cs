﻿using LibraryTask.Models;
using LibraryTask.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryTask.Controllers
{
    [Route("api/SqlLibrary/")]
    [ApiController]
    public class SqlLibraryController : ControllerBase
    {
        private readonly Func<IService<Domain.DomainBook>> _bookService;
        private readonly Func<IService<Domain.DomainAuthor>> _authorService;

        public SqlLibraryController(Func<IService<Domain.DomainBook>> bookService, Func<IService<Domain.DomainAuthor>> authorService)
        {
            _bookService = bookService;
            _authorService = authorService;
        }

        [HttpGet("getBooksList/")]
        public async Task<ActionResult> GetBooksList(int skip, int take)
        {
            return Ok(await _bookService().GetList(skip, take));
        }

        [HttpGet("getBooksCount/")]
        public async Task<ActionResult> getBooksCount()
        {
            var res = await _bookService().GetCount();
            return Ok(res);
        }

        [HttpGet("getBook/{id}")]
        public async Task<ActionResult> GetBook(Guid id)
        {
            var res = await _bookService().Get(id);
            return Ok(res);
        }

        [HttpPost("createBook/")]
        public async Task<ActionResult> CreateBook([FromBody] CreateEditBook model)
        {
            try
            {
                Domain.DomainBook book = new Domain.DomainBook()
                {
                    Title = model.title,
                    Genre = model.genre,
                    Year = model.year,
                    Publisher = model.publisher                        
                };

                return Ok(await _bookService().Create(book, model.authors));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("editBook/")]
        public async Task<ActionResult> EditBook([FromBody] CreateEditBook model)
        {
            try
            {
                Domain.DomainBook book = new Domain.DomainBook
                {
                    Id = model.id,
                    Title = model.title,
                    Genre = model.genre,
                    Year = model.year,
                    Publisher = model.publisher
                };

                return Ok(await _bookService().Edit(book, model.authors));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("deleteBook/{id}")]
        public async Task<ActionResult<int>> DeleteBook(Guid id)
        {
            try
            {
                 return Ok(await _bookService().Delete(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("getAuthorsList/")]
        public async Task<ActionResult> GetAuthorsList(int skip, int take)
        {
            return Ok(await _authorService().GetList(skip, take));
        }

        [HttpGet("getAuthorsCount/")]
        public async Task<ActionResult> GetAuthorsCount()
        {
            var res = await _authorService().GetCount();
            return Ok(res);
        }


        [HttpGet("getAuthor/{id}")]
        public async Task<ActionResult> GetAuthor(Guid id)
        {
            try
            {
                var result = await _authorService().Get(id);
                if (result != null)
                    return Ok(result);
                else
                    return BadRequest("No Such Author");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("createAuthor/")]
        public async Task<ActionResult> CreateAuthor([FromBody] CreateEditAuthor model)
        {
            try
            {
                Domain.DomainAuthor author = new Domain.DomainAuthor()
                {
                    FirstName = model.firstName,
                    LastName = model.lastName,
                    Country = model.country
                };
                return Ok(await _authorService().Create(author, model.books));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("editAuthor/")]
        public async Task<ActionResult> EditAuthor([FromBody] CreateEditAuthor model)
        {
            try
            {
                Domain.DomainAuthor author = new Domain.DomainAuthor()
                {
                    Id = model.id,
                    FirstName = model.firstName,
                    LastName = model.lastName,
                    Country = model.country
                };

                var result = await _authorService().Edit(author, model.books);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("deleteAuthor/{id}")]
        public async Task<ActionResult<int>> DeleteAuthor(Guid id)
        {
            try
            {
                return Ok(await _authorService().Delete(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
