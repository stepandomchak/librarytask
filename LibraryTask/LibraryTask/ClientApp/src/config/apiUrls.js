﻿export const apiUrls = {
    mongoApi: {
        url: "mongo/",
        name: "MongoDb"
    },
    sqlApi: {
        url: "sql/",
        name: "SqlDb"
        }
}