﻿import BookStore from './BookStore';
import AuthorStore from './AuthorStore';

const modelStores = () => {
    return {
        BookStore,
        AuthorStore
    };
};

export default modelStores();
