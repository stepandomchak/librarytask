﻿import { observable, decorate, runInAction } from "mobx";
import { apiUrls } from '../config/apiUrls';
import axios from 'axios';
import { notification } from 'antd';

class AuthorStore {
    isLoading = false;
    error = null;
    currentUrl = apiUrls.sqlApi;
    currentPage = 1;

    get curUrl() { return this.currentUrl; }

    authors = [];
    author = {};
    authorId = 0;
    authorsCount = 0;
    books = [];

    changeApi(url) {
        switch (url) {
            case apiUrls.mongoApi.url:
                this.currentUrl = apiUrls.mongoApi;
                break;
            default:
                this.currentUrl = apiUrls.sqlApi;

        }
    }

    loadAuthors = async (skip, take) => {
        try {
            const { data } = await axios.get(`https://localhost:44308/api/SqlLibrary/getauthorslist/?skip=${skip}&take=${take}`,
                { headers: { "DbType": this.currentUrl.name } });

            runInAction(() => {
                this.authors = data;
            });
        } catch (e) {
            this.error = e;
        }

    }

    loadAuthor= async (authorId) => {
        try {
            const { data } = await axios.get(`https://localhost:44308/api/SqlLibrary/getauthor/${authorId}`,
                { headers: { "DbType": this.currentUrl.name } });

            runInAction(() => {
                this.author = data;
                this.books = [];
                this.books = this.author.bookAuthor;
            });
        } catch (e) {
            this.error = e;
            console.log(e);
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }

    }

    createAuthor = async (author, books) => {
        try {
            const { data, ...params } = await axios({
                method: 'post',
                url: `https://localhost:44308/api/SqlLibrary/createauthor/`,
                data: { ...author, books},
                headers: {
                    'Content-Type': 'application/json',
                    'DbType': this.currentUrl.name
                }
            });            

            runInAction(() => {            
                this.author = data;
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }

    }

    updateAuthor = async (author, books) => {
        try {
            const { data, ...params } = await axios({
                method: 'put',
                url: `https://localhost:44308/api/SqlLibrary/editauthor/`,
                data: { ...author, books },
                headers: {
                    'Content-Type': 'application/json',
                    'DbType': this.currentUrl.name
                }
            });

            runInAction(() => {
                this.author = data;
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }

    }

    deleteAuthor = async (authorId) => {
        try {
            const { data, ...params } = await axios({
                method: 'delete',
                url: `https://localhost:44308/api/SqlLibrary/deleteauthor/${authorId}`,
                headers: { "DbType": this.currentUrl.name }
            });

            runInAction(() => {
                this.authors = this.authors.filter((author) => author.id !== data);
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }

    }

    getAuthorsCount = async () => {
        try {
            const { data } = await axios.get(`https://localhost:44308/api/SqlLibrary/getAuthorsCount/`,
                { headers: { "DbType": this.currentUrl.name } });

            runInAction(() => {
                this.authorsCount = data;
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }
    }

}

decorate(AuthorStore, {
    isLoading: observable,
    error: observable,
    authors: observable,
    author: observable,
    authorId: observable,
    currentUrl: observable,
    currentPage: observable,
    books: observable
});

export default new AuthorStore();

