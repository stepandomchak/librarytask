﻿import { observable, decorate, runInAction } from "mobx";
import { apiUrls } from '../config/apiUrls';
import axios from 'axios';
import { notification } from 'antd';

class BookStore {
    isLoading = false;
    error = null;
    currentUrl = apiUrls.sqlApi;
    currentPage = 1;

    get curUrl(){ return this.currentUrl; }

    books = [];
    book = {};
    bookId = 0;
    booksCount = 0;
    authors = [];

    changeApi(url) {
        switch (url) {
            case apiUrls.mongoApi.url:
                this.currentUrl = apiUrls.mongoApi;
                break;
            default:
                this.currentUrl = apiUrls.sqlApi;
                
        }
    }

    loadBooks = async (skip, take) => {
        try {
            const { data } = await axios.get(`https://localhost:44308/api/SqlLibrary/getbookslist/?skip=${skip}&take=${take}`,
                { headers: { "DbType": this.currentUrl.name } });

            runInAction(() => {
                this.books = data;
            });
        } catch (e) {
            this.error = e;
        } 

    }

    loadBook = async (bookId) => {
        try {
            const { data } = await axios.get(`https://localhost:44308/api/SqlLibrary/getbook/${bookId}`,
                { headers: { "DbType": this.currentUrl.name } });

            runInAction(() => {
                this.book = data;
                this.authors = [];
                this.authors = this.book.bookAuthor;
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }

    }

    createBook = async (book, authors) => {
        try {
            const { data, ...params } = await axios({
                method: 'post',
                url: `https://localhost:44308/api/SqlLibrary/createbook/`,
                data: { ...book, authors },
                headers: {
                    'Content-Type': 'application/json',
                    'DbType': this.currentUrl.name
                }
            });

            runInAction(() => {
                this.book = data;
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }

    }

    updateBook = async (book, authors) => {
        try {
            const { data, ...params } = await axios({
                method: 'put',
                url: `https://localhost:44308/api/SqlLibrary/editbook/`,
                data: { ...book, authors },
                headers: {
                    'Content-Type': 'application/json',
                    'DbType': this.currentUrl.name
                }
            });

            runInAction(() => {
                this.book = data;
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }

    }

    deleteBook = async (bookId) => {
        try {
            const { data, ...params } = await axios({
                method: 'delete',
                url: `https://localhost:44308/api/SqlLibrary/deletebook/${bookId}`,
                headers: { 'DbType': this.currentUrl.name }
            });

            runInAction(() => {
                this.books = this.books.filter((book) => book.id !== data);
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }

    }

    getBooksCount = async () => {
        try {
            const { data } = await axios.get(`https://localhost:44308/api/SqlLibrary/getBooksCount/`,
                { headers: { "DbType": this.currentUrl.name } });

            runInAction(() => {
                this.booksCount = data;
            });
        } catch (e) {
            this.error = e;
            notification.error({
                message: 'Error',
                description:
                    e.response.data
            });
        }
    }
}

decorate(BookStore, {
    isLoading: observable,
    error: observable,
    books: observable,
    booksCount: observable,
    book: observable,
    bookId: observable,
    currentUrl: observable,
    currentPage: observable,
    authors: observable
});

export default new BookStore();

