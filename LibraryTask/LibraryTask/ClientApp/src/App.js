import React, { Component } from 'react';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Route } from 'react-router-dom';
import { Book, CreateBookForm, UpdateBookForm, BooksList } from './components/Books';
import { Author, CreateAuthorForm, UpdateAuthorForm, AuthorsList } from './components/Authors';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
        <Layout>
            <Route exact path='/book/:bookId' render={(props) => <Book {...props}/>} />
            <Route exact path='/createBook' render={(props) => <CreateBookForm {...props}/>} />
            <Route exact path='/updateBook' render={(props) => <UpdateBookForm {...props} />} />
            <Route exact path='/BooksList' render={(props) => <BooksList {...props} />} />
            <Route exact path='/author/:authorId' render={(props) => <Author {...props} />} />
            <Route exact path='/createAuthor' render={(props) => <CreateAuthorForm {...props}/>} />
            <Route exact path='/updateAuthor' render={(props) => <UpdateAuthorForm {...props}/>} />
            <Route exact path='/AuthorsList' render={(props) => <AuthorsList {...props}/>} />
            <Route exact path='/' component={ Home }/>
        </Layout>
    );
  }
}
