﻿import React, { Component } from 'react';
import { inject, observer } from "mobx-react";
import { withRouter } from 'react-router-dom';
import { clone } from 'ramda';
import { Button, TextField, FormControl } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import Paper from '@material-ui/core/Paper';
import { notification, Select } from 'antd';
import 'antd/dist/antd.css'

const classes = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch'
        },
    },
}));
const formControl = {
    margin: '10px 0'
};
const { Option } = Select;

export const Author = withRouter(inject('AuthorStore', 'BookStore')(observer(class Author extends Component {

    constructor(props) {
        super(props);
        this.onEditClick = this.onEditClick.bind(this);
        this.state = {
            author: { firstName: "", lastName: "", country: "", books: [] },
        };
        this.onDeleteAuthor = this.onDeleteAuthor.bind(this);

    }

    onEditClick() {
        const books = this.props.AuthorStore.books;
        const booksList = this.props.BookStore.books;
        const arr = this.compare(booksList, books);
        this.props.history.push(`/updateAuthor/`, { authorId: this.props.history.location.state.authorId, authors: clone(this.props.AuthorStore.books), arr: clone(arr)  });
    }

    onDeleteAuthor() {
        this.props.AuthorStore.deleteAuthor(this.props.history.location.state.authorId);
        this.props.history.push("/authorsList");
    }

    compare(arr1, arr2) {
        let final = [];
        arr1.forEach((a1) => arr2.forEach((a2) => {
            if (a1.id === a2.id) {
                final.push(a1);
            }
        }))

        return final;
    }

    async componentDidMount() {
        await this.props.BookStore.getBooksCount();
        const { booksCount } = this.props.BookStore;

        if (booksCount != 0)
            await this.props.BookStore.loadBooks(0, booksCount);

        await this.props.AuthorStore.loadAuthor(this.props.history.location.state.authorId);
        this.setState({ author: this.props.AuthorStore.author || {} });
    }   

    render() {
        const children = [];

        const author = this.props.AuthorStore.author;
        const books = this.props.AuthorStore.books;

        for (let book of books) {
            children.push(<Option key={book.id} value={book.id}>{book.title}</Option>);            
        }

        return (
            <div>
                <form classes={classes.root}>
                    <FormControl>
                        <div style={{ display: "flex" }}>
                            <Button variant="contained" color="primary" onClick={this.onEditClick}>Edit Author</Button>
                            <Button style={{ marginLeft: "10px" }} variant="contained" color="secondary" onClick={() => this.onDeleteAuthor()}>Delete</Button>
                        </div>
                        <TextField style={{ margin: '10px 0' }} label="First Name" variant="outlined" InputProps={{ readOnly: true }} value={author.firstName || ''} />
                        <TextField style={{ margin: '10px 0' }} label="Last Name" variant="outlined" InputProps={{ readOnly: true }} value={author.lastName || ''} />
                        <TextField style={{ margin: '10px 0' }} label="Country" variant="outlined" InputProps={{ readOnly: true }} value={author.country || ''} />
                    </FormControl>
                    <div style={{ padding: '10px' }}>
                        <Select
                            mode="multiple"
                            disabled={true}
                            style={{ width: '100%', margin: '10px' }}
                            placeholder="Please select"
                            defaultValue={books.map(item => item.id)}
                            onChange={this.handleChange}>
                            {children}
                        </Select>
                    </div>
                </form>
            </div>
        )
    }
}
)));

export const UpdateAuthorForm = withRouter(inject('AuthorStore', 'BookStore')(observer(class AuthorForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            author: { firstName: "", lastName: "", country: "" },
            authorId: 0,
            books: []
        };
        this.onFirstNameChange = this.onFirstNameChange.bind(this);
        this.onLastNameChange = this.onLastNameChange.bind(this);
        this.onCountryChange = this.onCountryChange.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.updateAuthor = this.updateAuthor.bind(this);
    }

    async loadData() {
        await this.props.BookStore.getBooksCount();
        const { booksCount } = this.props.BookStore;

        if (booksCount != 0)
            await this.props.BookStore.loadBooks(0, booksCount);

        this.setState({ authorId: this.props.history.location.state.authorId || {} });

        await this.props.AuthorStore.loadAuthor(this.props.history.location.state.authorId);
        this.setState({ author: this.props.AuthorStore.author || {} });
        this.setState({ books: this.props.history.location.state.arr.map(item => item.id) });
    }

    onFirstNameChange(e) {
        this.setState({ author: { ...this.state.author, firstName: e.target.value } });
    }
    onLastNameChange(e) {
        this.setState({ author: { ...this.state.author, lastName: e.target.value } });
    }
    onCountryChange(e) {
        this.setState({ author: { ...this.state.author, country: e.target.value } });
    }

    handleChange(value) {
        this.setState({ books: value });        
    }

    async updateAuthor() {
        if (!this.state.author.firstName) {
            notification.error({
                message: 'Error',
                description: 'Fill FirstName'
            });
            return;
        }

        const firstName = this.state.author.firstName.trim();
        if (!firstName) {
            notification.error({
                message: 'Error',
                description: 'Fill Title'
            });
            return;
        }

        if (!this.state.author.lastName) {
            notification.error({
                message: 'Error',
                description: 'Fill LastName'
            });
            return;
        }

        const lastName = this.state.author.lastName.trim();
        if (!lastName) {
            notification.error({
                message: 'Error',
                description: 'Fill LastName'
            });
            return;
        }

        await this.props.AuthorStore.updateAuthor(this.state.author, this.state.books);
        this.props.history.push("/authorsList");
    }

    async componentDidMount() {
        await this.loadData();
    }

    render() {

        const author = this.state.author;
        const booksList = this.props.BookStore.books;

        const arr = this.props.history.location.state.arr;
        return (
            <div>
                <FormControl noValidate autoComplete="off">
                    <TextField style={{ margin: '10px 0' }} label="First Name" onChange={this.onFirstNameChange} value={author.firstName || '' } />
                    <TextField style={{ margin: '10px 0' }} label="Last Name" onChange={this.onLastNameChange} value={author.lastName || '' } />
                    <TextField style={{ margin: '10px 0' }} label="Country" onChange={this.onCountryChange} value={author.country || '' } />
                    <Button variant="contained" color="primary" onClick={this.updateAuthor}>Save</Button>
                </FormControl>
                <div style={{ padding: '10px' }}>
                    <Select
                        mode="multiple"
                        style={{ width: '100%', margin: '10px' }}
                        placeholder="Please select"
                        defaultValue={arr.map(item => item.id)}
                        onChange={this.handleChange}>
                        {
                            booksList.map((book) => {
                                return <Option key={book.id} value={book.id}>{book.title}</Option>;
                            })
                        }
                    </Select>
                </div>
            </div>
        )
    };
}
)));

export const CreateAuthorForm = withRouter(inject('AuthorStore', 'BookStore')(observer(class AuthorForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            author: { firstName: "", lastName: "", country: "", books: [] }
        };
        this.onFirstNameChange = this.onFirstNameChange.bind(this);
        this.onLastNameChange = this.onLastNameChange.bind(this);
        this.onCountryChange = this.onCountryChange.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.createAuthor = this.createAuthor.bind(this);
    }

    async loadData() {
        await this.props.BookStore.getBooksCount();
        const { booksCount } = this.props.BookStore;

        if (booksCount != 0)
            await this.props.BookStore.loadBooks(0, booksCount);

        this.setState({ author: this.props.history.location.state || {} });
    }

    onFirstNameChange(e) {
        this.setState({ author: { ...this.state.author, firstName: e.target.value } });
    }
    onLastNameChange(e) {
        this.setState({ author: { ...this.state.author, lastName: e.target.value } });
    }
    onCountryChange(e) {
        this.setState({ author: { ...this.state.author, country: e.target.value } });
    }

    handleChange(value) {
        this.setState({ books: value });        
    }

    async createAuthor() {
        if (!this.state.author.firstName) {
            notification.error({
                message: 'Error',
                description: 'Fill FirstName'
            });
            return;
        }

        const firstName = this.state.author.firstName.trim();
        if (!firstName) {
            notification.error({
                message: 'Error',
                description: 'Fill Title'
            });
            return;
        }

        if (!this.state.author.lastName) {
            notification.error({
                message: 'Error',
                description: 'Fill LastName'
            });
            return;
        }

        const lastName = this.state.author.lastName.trim();
        if (!lastName) {
            notification.error({
                message: 'Error',
                description: 'Fill LastName'
            });
            return;
        }

        await this.props.AuthorStore.createAuthor(this.state.author, this.state.books);
        this.props.history.push("/authorsList");
    }

    async componentDidMount() {
        await this.loadData();

        const locationState = this.props.history.location.state
        this.setState({
            author: locationState ? locationState.author : {}
        });
    }

    render() {
        const children = [];
        const author = this.state.author;

        const booksList = this.props.BookStore.books;

        for (let book of booksList) {
            children.push(<Option key={book.id} value={book.id}>{book.title}</Option>);
        }

            return (
                <div>
                    <FormControl noValidate autoComplete="off">
                        <TextField style={{ margin: '10px 0' }} label="First Name" required onChange={this.onFirstNameChange} value={author.firstName || ''} />
                        <TextField style={{ margin: '10px 0' }} label="Last Name" required onChange={this.onLastNameChange} value={author.lastName || ''} />
                        <TextField style={{ margin: '10px 0' }} label="Country" onChange={this.onCountryChange} value={author.country || ''} />
                        <Button variant="contained" color="primary" onClick={this.createAuthor}>Create</Button>
                    </FormControl>
                    <div style={{ padding: '10px' }}>
                        <Select
                            mode="multiple"
                            style={{ width: '100%', margin: '10px' }}
                            placeholder="Please select"
                            onChange={this.handleChange}>
                            {children}
                        </Select>
                    </div>
                </div>
            )
    }

}
)));

export const AuthorsList = withRouter(inject('AuthorStore')(observer(class AuthorsList extends Component {
    constructor(props) {
        super(props);
        this.state = { authors: [], currentUrl: null, authorsPerPage: 5, currentPage: 1  };

        this.onAddAuthor = this.onAddAuthor.bind(this);
        this.onDetailsAuthor = this.onDetailsAuthor.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    async loadData() {
        const skip = (this.state.currentPage - 1) * this.state.authorsPerPage

        await this.props.AuthorStore.loadAuthors(skip, this.state.authorsPerPage);
        await this.props.AuthorStore.getAuthorsCount();
    }

    onAddAuthor() {
        this.props.history.push(`/createAuthor/`);
    }

    async onDetailsAuthor(authorId) {
        await this.props.AuthorStore.loadAuthor(authorId);
        const author = this.props.AuthorStore.author;
        this.props.history.push(`/author/${authorId}`, { authorId: authorId });
    }

    async componentDidMount() {
        await this.loadData();
    }

    async componentDidUpdate(prevProps) {
        if (this.state.currentUrl !== this.props.AuthorStore.curUrl) {
            this.setState({ currentUrl: this.props.AuthorStore.curUrl });
            await this.loadData();
        }
        if (this.state.currentPage !== this.props.AuthorStore.currentPage) {
            this.props.AuthorStore.currentPage = this.state.currentPage;
            await this.loadData();
        }
    }

    async handleChange(event, value) {
        this.setState({ currentPage: value });
    };
    render() {
        const { authors, authorsCount, currentUrl } = this.props.AuthorStore;
        const totalPages = Math.ceil(authorsCount / this.state.authorsPerPage);

        return (<div>
            <Button variant="contained" color="primary" onClick={this.onAddAuthor}>Create Author</Button>

            <h2>Authors List</h2>
            <div>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>FirstName</TableCell>
                                <TableCell>LastName</TableCell>
                                <TableCell>Country</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {
                            authors !== null && authors.map(author =>
                                <TableRow key={author.id}>
                                    <TableCell>{author.firstName}</TableCell>
                                    <TableCell>{author.lastName}</TableCell>
                                    <TableCell>{author.country}</TableCell>
                                    <TableCell>
                                        <Button variant="contained" color="default" onClick={() => this.onDetailsAuthor(author.id)}>Details</Button>
                                    </TableCell>
                                </TableRow>
                            )
                        }
                        </TableBody>
                    </Table>
                </TableContainer>
                <Pagination color="primary" count={totalPages} onChange={this.handleChange}>
                </Pagination>
            </div>
        </div>)
    }

}
)));