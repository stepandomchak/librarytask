﻿import React, { Component } from 'react';
import { inject, observer } from "mobx-react";
import { withRouter } from 'react-router-dom';
import { clone } from 'ramda';
import { Button, TextField, FormControl }from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Pagination from '@material-ui/lab/Pagination';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import moment from 'moment';
import { notification, Select, DatePicker } from 'antd';
import 'antd/dist/antd.css'

const classes = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch'
        },
    },
}));
const formControl = {
    margin: '10px 0'
};
const { Option } = Select;

export const Book = withRouter(inject('BookStore', 'AuthorStore')(observer(class Book extends Component {

    constructor(props) {
        super(props);
        this.onEditClick = this.onEditClick.bind(this);
        this.onDeleteBook = this.onDeleteBook.bind(this);
        this.state = {
            book: { title: "", genre: "", year: "", publisher: "", authors: [] },
        };

    }

    onDeleteBook() {
        this.props.BookStore.deleteBook(this.props.history.location.state.bookId);
        this.props.history.push(`/booksList/`);
    }

    onEditClick() {
        const authors = this.props.BookStore.authors;
        const authorsList = this.props.AuthorStore.authors;
        const arr = this.compare(authorsList, authors);

        this.props.history.push(`/updateBook/`, { bookId: this.props.history.location.state.bookId, authors: clone(this.props.BookStore.authors), arr: clone(arr) });
    }

    compare(arr1, arr2) {
        let final = [];
        arr1.forEach((a1) => arr2.forEach((a2) => {
            if (a1.id === a2.id) {
                final.push(a1);
            }
        }))

        return final;
    }

    async componentDidMount() {
        await this.props.AuthorStore.getAuthorsCount();
        const { authorsCount } = this.props.AuthorStore;

        if (authorsCount != 0)
            await this.props.AuthorStore.loadAuthors(0, authorsCount);

        await this.props.BookStore.loadBook(this.props.history.location.state.bookId);
        this.setState({ book: this.props.BookStore.book || {}});
    }

    render() {
        const children = [];

        const book = this.props.BookStore.book;
        const authors = this.props.BookStore.authors;
        for (let item of authors) {
            children.push(<Option key={item.id} value={item.id}>{item.firstName + " " + item.lastName}</Option>);
        }

        return (
            <div>
                <form classes={classes.root}>
                    <FormControl>
                        <div>
                            <Button variant="contained" color="primary" onClick={this.onEditClick}>Edit Book</Button>
                            <Button variant="contained" style={{marginLeft: "10px"}} color="secondary" onClick={() => this.onDeleteBook()}>Delete</Button>
                        </div>
                        <TextField style={formControl} label="Title" variant="outlined" InputProps={{ readOnly: true }} value={book.title || ''} />
                        <TextField style={formControl} label="Genre" variant="outlined" InputProps={{ readOnly: true }} value={book.genre || ''} />
                        <TextField style={formControl} label="Year" variant="outlined" InputProps={{ readOnly: true }} value={moment(book.year).format('YYYY-MM-DD') || ''} />
                        <TextField style={formControl} label="Publisher" variant="outlined" InputProps={{ readOnly: true }} value={book.publisher || ''} />
                    </FormControl>
                    <div style={{ padding: '10px' }}>
                        <Select
                            mode="multiple"
                            disabled={true}
                            style={{ width: '100%', margin: '10px' }}
                            placeholder="Please select"
                            defaultValue={authors.map(item => item.id)}>
                            {children}
                        </Select>
                    </div>
                </form>
            </div>
        )
    }
}
))); 

export const UpdateBookForm = withRouter(inject('BookStore', 'AuthorStore')(observer(class BookForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            book: { title: "", genre: "", year: "", publisher: "" },
            bookId: 0,
            authors: []
        };

        this.onTitleChange = this.onTitleChange.bind(this);
        this.onGenreChange = this.onGenreChange.bind(this);
        this.onYearChange = this.onYearChange.bind(this);
        this.onPublisherChange = this.onPublisherChange.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.updateBook = this.updateBook.bind(this);
    }

    async loadData() {
        await this.props.AuthorStore.getAuthorsCount();
        const { authorsCount } = this.props.AuthorStore;

        if (authorsCount != 0)
            await this.props.AuthorStore.loadAuthors(0, authorsCount);
        
        this.setState({ bookId: this.props.history.location.state.bookId|| {} });

        await this.props.BookStore.loadBook(this.props.history.location.state.bookId);
        this.setState({ book: this.props.BookStore.book || {} });
        this.setState({ authors: this.props.history.location.state.arr.map(item => item.id) });
    }

    onTitleChange(e) {
        this.setState({ book: { ...this.state.book, title: e.target.value } });
    }
    onGenreChange(e) {
        this.setState({ book: { ...this.state.book, genre: e.target.value } });
    }
    onYearChange(e) {
        if(e != null)
            this.setState({ book: { ...this.state.book, year: moment(e) } });
    }
    onPublisherChange(e) {
        this.setState({ book: { ...this.state.book, publisher: e.target.value } });
    }

    handleChange(value) {        
        this.setState({ authors: value });        
    }

    async updateBook() {
        if (!this.state.book.title) {
            notification.error({
                message: 'Error',
                description: 'Fill Title'
            });
            return;
        }

        const text = this.state.book.title.trim();
        if (!text) {
            notification.error({
                message: 'Error',
                description: 'Fill Title'
            });
            return;
        }

        await this.props.BookStore.updateBook(this.state.book, this.state.authors);
        this.props.history.push("/booksList");
    }

    async componentDidMount() {
        await this.loadData();
    }    

    render() {

        const book = this.state.book;    
        const authorsList = this.props.AuthorStore.authors;

        const arr = this.props.history.location.state.arr;
        return (
            <div>
                <FormControl noValidate autoComplete="off">
                    <TextField style={formControl} label="Title" required onChange={this.onTitleChange} value={book.title || ''} />
                    <TextField style={formControl} label="Genre" required onChange={this.onGenreChange} value={book.genre || ''} />
                    <DatePicker style={formControl} label="Year" required onChange={this.onYearChange} value={moment(book.year) || ''} />
                    <TextField style={formControl} label="Publisher" required onChange={this.onPublisherChange} value={book.publisher || ''} />
                    <Button style={formControl} variant="contained" color="primary" onClick={this.updateBook}>Save</Button>
                </FormControl>
                <div style={{ padding: '10px' }}>                    
                    <Select
                        mode="multiple"
                        style={{ width: '100%', margin: '10px' }}
                        placeholder="Please select"                           
                        defaultValue={arr.map(item=>item.id)}
                        onChange={this.handleChange}>
                        {
                            authorsList.map((author) => {
                                return <Option key={author.id} value={author.id}>{author.firstName + " " + author.lastName}</Option>;
                            })
                        }
                    </Select>
                </div>
            </div>
        );
    };
}
)));

export const CreateBookForm = withRouter(inject('BookStore', 'AuthorStore')(observer(class BookForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            book: { title: "", genre: "", year: "", publisher: "", authors: [] },
        };

        this.onTitleChange = this.onTitleChange.bind(this);
        this.onGenreChange = this.onGenreChange.bind(this);
        this.onYearChange = this.onYearChange.bind(this);
        this.onPublisherChange = this.onPublisherChange.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.createBook = this.createBook.bind(this);
    }

    async loadData() {
        await this.props.AuthorStore.getAuthorsCount();
        const { authorsCount } = this.props.AuthorStore;

        if (authorsCount != 0)
            await this.props.AuthorStore.loadAuthors(0, authorsCount);

        this.setState({ book: this.props.history.location.state || {} });
    }

    onTitleChange(e) {
        this.setState({ book: { ...this.state.book, title: e.target.value } });
    }
    onGenreChange(e) {
        this.setState({ book: { ...this.state.book, genre: e.target.value } });
    }
    onYearChange(e) {
        this.setState({ book: { ...this.state.book, year: moment(e) } });
    }
    onPublisherChange(e) {
        this.setState({ book: { ...this.state.book, publisher: e.target.value } });
    }

    handleChange(value) {
        this.setState({ authors: value });        
    }

    async createBook() {
        if (!this.state.book.title) {
            notification.error({
                message: 'Error',
                description: 'Fill Title'
            });
            return;
        }

        const text = this.state.book.title.trim();
        if (!text) {
            notification.error({
                message: 'Error',
                description: 'Fill Title'
            });
            return;
        }

        await this.props.BookStore.createBook(this.state.book, this.state.authors);
        this.props.history.push("/booksList");
    }

    async componentDidMount() {
        await this.loadData();

        const locationState = this.props.history.location.state
        this.setState({
            book: locationState ? locationState.book : {}
        });
        this.setState({ book: { ...this.state.book, year: moment(Date.now()) } });
    }

    render() {
        const children = [];
        const book = this.state.book;

        const authorsList = this.props.AuthorStore.authors;

        for (let author of authorsList) {
            children.push(<Option key={author.id} value={author.id}>{author.firstName + " " + author.lastName}</Option>);
        }
            return (
                <div>
                    <FormControl autoComplete="off">
                        <div>
                            <TextField style={formControl} label="Title" required onChange={this.onTitleChange} value={book.title || ''} />
                            <TextField style={formControl} label="Genre" onChange={this.onGenreChange} value={book.genre || ''} />
                            <DatePicker style={formControl} label="Year" required onChange={this.onYearChange} value={moment(book.year) || ''} />
                            <TextField style={formControl} label="Publisher" onChange={this.onPublisherChange} value={book.publisher || ''} />         
                            <Button style={formControl} variant="contained" color="primary" onClick={this.createBook}>Create</Button>
                        </div>
                    </FormControl>
                    <div style={{ padding: '10px' }}>
                        <Select
                            mode="multiple"
                            style={{ width: '100%', margin: '10px' }}                            
                            placeholder="Please select"
                            onChange={this.handleChange}>
                            {children}
                        </Select>
                    </div>
                </div>
            )        
    }
}
)));

export const BooksList = withRouter(inject('BookStore')(observer(class BooksList extends Component {

    constructor(props) {
        super(props);
        this.state = { books: [], currentUrl: null, booksPerPage: 5, currentPage: 1 };

        this.onAddBook = this.onAddBook.bind(this);
        this.onDetailsBook = this.onDetailsBook.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    async loadData() {
        const skip = (this.state.currentPage - 1) * this.state.booksPerPage
        
        await this.props.BookStore.loadBooks(skip, this.state.booksPerPage);
        await this.props.BookStore.getBooksCount();
    }

    onAddBook() {
        this.props.history.push(`/createBook`);
    }

    async onDetailsBook(bookId) {
        await this.props.BookStore.loadBook(bookId);
        const book = this.props.BookStore.book;
        this.props.history.push(`/book/${bookId}`, { bookId: bookId });
    }

    async componentDidMount() {
        await this.loadData();
    }

    async componentDidUpdate(prevProps) {     
        if (this.state.currentUrl !== this.props.BookStore.curUrl) {
            this.setState({ currentUrl: this.props.BookStore.curUrl });
            await this.loadData();
        }

        if (this.state.currentPage !== this.props.BookStore.currentPage) {
            this.props.BookStore.currentPage = this.state.currentPage;
            await this.loadData();
        }
    }

    async handleChange(event, value) {
        this.setState({ currentPage: value });
    };

    render() {
        const { books, booksCount, currentUrl } = this.props.BookStore;
        const totalPages = Math.ceil(booksCount / this.state.booksPerPage);
        return (<div>
            <Button variant="contained" color="primary" onClick={this.onAddBook}>Create Book</Button>

            <h2>Books List</h2>
            <div>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Title</TableCell>
                                <TableCell>Genre</TableCell>
                                <TableCell>Year</TableCell>
                                <TableCell>Publisher</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                books !== null && books.map(book =>
                                    <TableRow key={book.id}>
                                        <TableCell>{book.title}</TableCell>
                                        <TableCell>{book.genre}</TableCell>
                                        <TableCell>{moment(book.year).format('YYYY-MM-DD')}</TableCell>
                                        <TableCell>{book.publisher}</TableCell>
                                        <TableCell>
                                            <Button variant="contained" color="default" onClick={() => this.onDetailsBook(book.id)}>Details</Button>
                                        </TableCell>
                                    </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
                <Pagination color="primary" count={totalPages} onChange={this.handleChange}>
                </Pagination>
            </div>
        </div>)
    }
}
)));
