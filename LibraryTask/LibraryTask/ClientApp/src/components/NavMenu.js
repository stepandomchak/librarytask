import React, { Component } from 'react';
import { inject, observer } from "mobx-react";
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';
import { Button, Select, MenuItem } from '@material-ui/core';
import { apiUrls } from '../config/apiUrls';
import { withRouter } from 'react-router-dom';

const navControl = {
    marginRight: '10px'
};

const NavMenu = withRouter(inject('BookStore', 'AuthorStore')(observer(class NavMenu extends Component {

    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true,
            api: true
        };

        this.changeApi = this.changeApi.bind(this);
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    changeApi(e) {        
        this.props.BookStore.changeApi(e.target.value);
        this.props.AuthorStore.changeApi(e.target.value);
    }

    render() {
        return (
            <header>
                <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
                    <Container>
                        <NavbarBrand tag={Link} to="/">LibraryTask</NavbarBrand>
                        <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
                            <ul className="navbar-nav flex-grow">
                                <NavItem>
                                    <Button style={navControl}  variant="contained" color="primary" onClick={() => this.props.history.push('/BooksList')} size="small">Books</Button>
                                </NavItem>
                                <NavItem>
                                    <Button style={navControl}  variant="contained" color="primary" onClick={() => this.props.history.push('/AuthorsList')} size="small">Authors</Button>
                                </NavItem>
                                <NavItem>
                                    <Select value={this.props.BookStore.curUrl.url} onChange={this.changeApi}>
                                        <MenuItem value={apiUrls.sqlApi.url}>{apiUrls.sqlApi.name}</MenuItem>
                                        <MenuItem value={apiUrls.mongoApi.url}>{apiUrls.mongoApi.name}</MenuItem>
                                    </Select>
                                </NavItem>
                            </ul>
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
    }
}

)));
export default NavMenu