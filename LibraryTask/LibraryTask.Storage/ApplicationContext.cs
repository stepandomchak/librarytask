﻿using LibraryTask.Models.SqlModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryTask.Storage
{
    public class ApplicationContext : DbContext
    {
        public DbSet<BookModel> Books { get; set; }

        public DbSet<AuthorModel> Authors { get; set; }
        

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthorModel>()
                .HasKey(s => s.Id);

            modelBuilder.Entity<AuthorModel>()
                .HasIndex(s => new { s.FirstName, s.LastName })
                .IsUnique(true);

            modelBuilder.Entity<BookAuthorModel>()
                .HasOne(sc => sc.Author)
                .WithMany(s => s.BookAuthor)
                .HasForeignKey(sc => sc.AuthorId);

            modelBuilder.Entity<BookAuthorModel>()
                .HasOne(sc => sc.Book)
                .WithMany(s => s.BookAuthor)
                .HasForeignKey(sc => sc.BookId);

            modelBuilder.Entity<BookAuthorModel>()
                .HasIndex(s => new { s.BookId, s.AuthorId })
                .IsUnique(true);

        }
    }

}
